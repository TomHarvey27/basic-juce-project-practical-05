/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    textButton1.setButtonText("click");
    addAndMakeVisible(textButton1);
    
    textButton2.setButtonText("click2");
    addAndMakeVisible(textButton2);
    
    slider1.setSliderStyle (Slider::LinearVertical);
    slider1.setRange(5000.0, 10000.0);
    addAndMakeVisible(slider1);
    
    comboBox1.addItem ("Item1", 1);
    comboBox1.addItem ("Item2", 2);
    comboBox1.addItem ("Item3", 3);
    addAndMakeVisible(comboBox1);
    
    textEditor1.setText ("Add some text here");
    addAndMakeVisible(textEditor1);
    
    textButton1.addListener (this);
    textButton2.addListener(this);
    slider1.addListener(this);
    
}

MainComponent::~MainComponent()
{

    
}

void MainComponent::resized()
{
    
    // Makes everything relative to eachother using rectangle
    
    Rectangle<int> r (getLocalBounds().reduced (10));
    
    textButton1.setBounds (r.removeFromTop (40));
    textButton2.setBounds (r.removeFromTop (40));
    slider1.setBounds (r.removeFromTop (40));
    comboBox1.setBounds (r.removeFromTop (40));
    textEditor1.setBounds (r.removeFromTop (40));

    //DBG ("Print any old random text");

}

void MainComponent::buttonClicked (Button* button)
{
    if (button == &textButton1)
    {
    
    DBG ("Button Clicked\n");
        
    }
    
    else
    {
        
        DBG ("Wrong button");
    }
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    
    DBG ("Slider moved");
}

void MainComponent::paint (Graphics& g)
{
    DBG ("window resized");
    
    
    g.drawEllipse (100, 0, getWidth(), getHeight(), 1);
    
}